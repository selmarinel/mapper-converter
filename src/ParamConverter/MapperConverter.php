<?php

namespace Mapper\ParamConverter;

use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use Mapper\Exception\ValidationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
/**
 * Class MapperConverter
 *
 * @author Selmarinel Nerdjin <selmarinel@gmail.com>
 */
class MapperConverter implements ParamConverterInterface
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * RequestParamConverter constructor.
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param Request $request
     * @param ParamConverter $configuration
     *
     * @return bool|void
     *
     * @throws ValidationException
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $groups = $this->prepareGroups($configuration);

        $build = SerializerBuilder::create()->build();
        //todo think about it ..
        $serialized = $build->serialize(
            $this->prepareRequestData($request),
            'json',
            $this->prepareSerializationContext($groups)
        );

        $vo = $build->fromArray(json_decode($serialized, true), $configuration->getClass());
        $this->validate($vo, $groups);

        $request->attributes->set($configuration->getName(), $vo);
    }

    /**
     * @param ParamConverter $configuration
     *
     * @return array|null
     */
    private function prepareGroups(ParamConverter $configuration): ?array
    {
        if (isset($configuration->getOptions()['groups']) && !empty($configuration->getOptions()['groups'])) {
            return $configuration->getOptions()['groups'];
        }

        return null;
    }


    /**
     * @param array|null $groups
     * @return SerializationContext
     */
    private function prepareSerializationContext(?array $groups = null): SerializationContext
    {
        $context = SerializationContext::create();
        if ($groups) {
            $context->setGroups($groups);
        }

        return $context;
    }

    /**
     * @param $object
     * @param array|null $groups
     *
     * @throws ValidationException
     */
    private function validate($object, ?array $groups): void
    {
        $violations = $this->validator->validate($object, null, $groups);
        if ($violations->count()) {
            throw new ValidationException($violations);
        }
    }

    /**
     * @param ParamConverter $configuration
     *
     * @return bool True if the object is supported, else false
     */
    public function supports(ParamConverter $configuration)
    {
        return $configuration->getConverter() === 'mapper_converter';
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    private function prepareRequestData(Request $request): array
    {
        $jsonBodyData = [];

        if ($request->getContentType() === 'json') {
            @$jsonBodyData = (array)json_decode($request->getContent(), true);
        }

        $requestData = $request->request->all();
        $routeParams = (array)$request->attributes->filter('_route_params');
        $getParameters = $request->query->all();

        return array_merge($jsonBodyData, $requestData, $routeParams, $getParameters);
    }
}