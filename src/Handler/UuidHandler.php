<?php

namespace Mapper\Handler;

use JMS\Serializer\GraphNavigatorInterface;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Visitor\SerializationVisitorInterface;
use Ramsey\Uuid\UuidInterface;

/**
 * Class UuidHandler
 */
class UuidHandler implements SubscribingHandlerInterface
{
    /**
     * @return array
     */
    public static function getSubscribingMethods()
    {
        return [
            [
                'direction' => GraphNavigatorInterface::DIRECTION_SERIALIZATION,
                'format' => 'json',
                'type' => 'uuid',
                'method' => 'serialize',
            ]
        ];
    }

    /**
     * @param SerializationVisitorInterface $visitor
     * @param UuidInterface $uuid
     *
     * @param array $type
     * @param SerializationContext $context
     * @return string
     */
    public function serialize(
        SerializationVisitorInterface $visitor,
        UuidInterface $uuid,
        array $type,
        SerializationContext $context
    ) {
        return $uuid->toString();
    }
}