<?php

namespace Mapper;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class MapperConverterBundle
 * @author Selmarinel Nerdjin <selmarinel@gmail.com>
 */
class MapperConverterBundle extends Bundle
{

}