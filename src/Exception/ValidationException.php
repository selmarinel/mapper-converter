<?php

namespace Mapper\Exception;

use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * Class ValidationException
 */
class ValidationException extends \Exception
{
    /**
     * @var ConstraintViolationListInterface
     */
    private $violationList;

    public function __construct(ConstraintViolationListInterface $violationList, Throwable $previous = null)
    {
        $this->violationList = $violationList;
        parent::__construct((string) $violationList,0, $previous);
    }

    /**
     * @return ConstraintViolationListInterface
     */
    public function getViolationList(): ConstraintViolationListInterface
    {
        return $this->violationList;
    }
}